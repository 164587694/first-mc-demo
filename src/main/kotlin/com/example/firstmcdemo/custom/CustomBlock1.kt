package com.example.firstmcdemo.custom

import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.ShapeContext
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.text.Text
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.shape.VoxelShape
import net.minecraft.util.shape.VoxelShapes
import net.minecraft.world.BlockView
import net.minecraft.world.World


class CustomBlock1(settings: Settings?) : Block(settings) {
    init {
        //`strength` 会将方块的硬度和抗性设为同一个值。
        // 硬度决定了方块需要多久挖掘，抗性决定了方块抵御爆破伤害（如爆炸）的能力。
        // 石头的硬度为 1.5f，抗性为 6.0f，黑曜石的硬度为 50.0f，抗性为 1200.0f。
        settings?.strength(1.5f)
        settings?.requiresTool()
    }

    override fun onUse(state: BlockState?, world: World, pos: BlockPos?, player: PlayerEntity, hand: Hand?, hit: BlockHitResult?): ActionResult {
        if (!world.isClient) {
            player.sendMessage(Text.literal("Hello, here!"), false);
        }

        return ActionResult.SUCCESS;
    }


    override fun getOutlineShape(state: BlockState?, view: BlockView?, pos: BlockPos?, context: ShapeContext?): VoxelShape {
        return VoxelShapes.cuboid(0.0, 0.0, 0.0, 1.0, 1.0, 0.5)
    }
}