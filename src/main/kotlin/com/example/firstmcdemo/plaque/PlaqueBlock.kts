package com.example.firstmcdemo.custom

import net.minecraft.block.Block
import sun.tools.jstat.Identifier

import java.rmi.registry.Registry


class PlaqueBlock(settings: Settings?) : Block(Settings.copy(Blocks.OAK_SIGN)) {

    // Register the block with a custom item and the correct texture
    val PLAQUE_BLOCK: PlaqueBlock = PlaqueBlock()

    val YOUR_CUSTOM_SIGN_BLOCK_ITEM: BlockItem = BlockItem(PLAQUE_BLOCK, Settings().group(ItemGroup.DECORATIONS))

    fun register() {
        Registry.register(Registry.BLOCK, Identifier("reoned", "plaque_block"), YOUR_CUSTOM_SIGN_BLOCK)
        Registry.register(Registry.ITEM, Identifier("reoned", "plaque_block"), YOUR_CUSTOM_SIGN_BLOCK_ITEM)
    }
}