package com.example.firstmcdemo

import com.example.firstmcdemo.custom.CustomBlock1
import com.example.firstmcdemo.custom.CustomItem1
import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.fabricmc.fabric.api.registry.FuelRegistry
import net.minecraft.block.Block
import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.item.ItemGroups
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.registry.RegistryKeys
import net.minecraft.registry.tag.TagKey
import net.minecraft.util.Identifier
import org.slf4j.LoggerFactory


object Firstmcdemo : ModInitializer {
    private val logger = LoggerFactory.getLogger("firstmcdemo")
	val tagKey = TagKey.of(RegistryKeys.BLOCK, Identifier("reoned", "custom_block_1"))

	override fun onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.
		logger.info("Hello Fabric world!")
		addItem()
		addBlock()
	}

	/**
	 * 添加一个物品
	 */
	private fun addItem() {
		val customItem1: Item = CustomItem1(FabricItemSettings())

		Registry.register(Registries.ITEM, Identifier("reoned", "custom_item_1"), customItem1)
		FuelRegistry.INSTANCE.add(customItem1, 300) //物品能作为燃料
		//CompostingChanceRegistry 可堆肥

		//添加到建筑方块物品组。原版物品组存储在 ItemGroups 类中
		ItemGroupEvents.modifyEntriesEvent(ItemGroups.BUILDING_BLOCKS).register {
			it.add(customItem1)
		}
	}

	/**
	 * 添加一个方块
	 */
	private fun addBlock() {
		val customBlock1: Block = CustomBlock1(FabricBlockSettings.create())
		Registry.register(Registries.BLOCK, Identifier("reoned", "custom_block_1"), customBlock1)
		//注册一个方块对应的物品，以便可以拿着
		Registry.register(Registries.ITEM, Identifier("reoned", "custom_block_1"), BlockItem(customBlock1, FabricItemSettings()))
	}
}