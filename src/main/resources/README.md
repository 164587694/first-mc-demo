## 添加物品纹理
1. 物品模型: resources/assets/tutorial/models/item/custom_item.json
2. 物品纹理: resources/assets/tutorial/textures/item/custom_item.png

## 合成配方
1. resources/data/tutorial/recipes/custom_item.json

## 给予方块外观
你可能已经注意到，新的方块只是游戏中紫色和黑色棋盘格图案。这表明Minecraft加载方块资源或外观时出错。运行客户端时，完整的问题列表会输出在你的日志中。你需要以下文件来给予方块外观：
1. 方块状态：assets/tutorial/blockstates/custom_block.json
2. 方块模型：assets/tutorial/models/block/custom_block.json
3. 物品模型：assets/tutorial/models/item/custom_block.json
4. 方块纹理：assets/tutorial/textures/block/custom_block.png

## 配置方块掉落物
1. src/main/resources/data/tutorial/loot_tables/blocks/custom_block.json

## 定义采集工具和采集等级
1. 采集工具：resources/data/minecraft/tags/blocks/mineable/<tooltype<.json 其中<tooltype<可以是 axe（斧头）、pickaxe（镐）、shovel（铲子）、hoe（锄头）
2. 采集等级：resources/data/minecraft/tags/blocks/needs_<tier<_tool.json，其中 <tier< 可以是：stone、iron、diamond（不包括 netherite）